$(document).ready(function() {
    var $container = $("div#note_links");
    //prompt("Hi");
    var $addLink = $("<a href=\"#\" id=\"add_link\" class=\"add-link-button\"><i class=\"fas fa-link\"></i></a>");
    $("div#buttons-container").append($addLink);
    $addLink.click(function(e){
        addKeyword($container);
        e.preventDefault();
        return false;
    });
    var index = $container.find(':input').length;
    if(index == 0) {
        //addKeyword($container);
    }
    else {
        $container.children("div").each(function() {
            addSuppressionLink($(this));
        });
    }
    function addKeyword($container){
        var $prototype = $($container.attr('data-prototype').replace(/__name__label__/g, '').replace(/__name__/g, index));
        addSuppressionLink($prototype);
        $container.append($prototype);
        //$prototype.insertAfter($addLink);
        index++;
        // Added to make textarea autoresized after adding a new one
        //$('textarea').autoResize();
        //$("textarea").height( $("textarea")[0].scrollHeight );
    }
    function addSuppressionLink($prototype) {
        $suppressionLink = $("<a href=\"#\"><div id=\"delete_link\" class=\"remove-link-button\" style=\"\"><i class=\"fas fa-trash-alt\"></i></div></a>");
        var $linkAnchor = $("<a href=\"#\"><div class=\"visit-link-button\" style=\"\"><i class=\"fas fa-link\"></i></div></a>");
        $prototype.prepend($linkAnchor);
        $prototype.append($suppressionLink);
        $suppressionLink.click(function(e) {
            $prototype.remove();
            e.preventDefault();
            return false;
        });
    }
});