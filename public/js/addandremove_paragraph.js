$(document).ready(function() {
    var $container = $("div#note_paragraphs");
    //prompt("Hi");
    var $addLink = $("<a href=\"#\" id=\"add_paragraph\" class=\"add-paragraph-button\"><i class=\"fas fa-align-justify\"></i></a>");
    $container.prepend($addLink);
    $addLink.click(function(e){
        addKeyword($container);
        e.preventDefault();
        return false;
    });
    var index = $container.find(':input').length;
    if(index == 0) {
        //addKeyword($container);
    }
    else {
        $container.children("div").each(function() {
            addSuppressionLink($(this));
        });
    }
    function addKeyword($container){
        var $prototype = $($container.attr('data-prototype').replace(/__name__label__/g, '').replace(/__name__/g, index));
        addSuppressionLink($prototype);
        $container.append($prototype);
        //$prototype.insertAfter($addLink);
        index++;
        // Added to make textarea autoresized after adding a new one
        $('textarea').autoResize();
        $("textarea").height( $("textarea")[0].scrollHeight );
    }
    function addSuppressionLink($prototype) {
        $suppressionLink = $("<a href=\"#\"><div id=\"delete_paragraph\" class=\"remove-paragraph-button\" style=\"\"><i class=\"fas fa-trash-alt\"></i></div></a>");
        $prototype.prepend($suppressionLink);
        $suppressionLink.click(function(e) {
            $prototype.remove();
            e.preventDefault();
            return false;
        });
    }
});