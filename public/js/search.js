function filter() {
    var input = document.getElementById("search");
    var filter = input.value.toLowerCase();
    var notesContainer = document.getElementById("notes-container");
    var notes = notesContainer.getElementsByClassName("note");
    for (i = 0 ; i < notes.length ; i++) {
        var title = notes[i].getElementsByClassName("title")[0];
        if (title) {
            var txtValue = title.textContent || title.innerText;
            if (txtValue.toLowerCase().indexOf(filter) > -1) {
                notes[i].style.display = "block";
            } else {
                notes[i].style.display = "none";
            }
        }
    }
}