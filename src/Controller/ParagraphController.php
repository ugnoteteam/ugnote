<?php

namespace App\Controller;

use App\Entity\Paragraph;
use App\Form\ParagraphType;
use App\Repository\ParagraphRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/paragraph")
 */
class ParagraphController extends AbstractController
{
    /**
     * @Route("/", name="paragraph_index", methods="GET")
     */
    public function index(ParagraphRepository $paragraphRepository): Response
    {
        return $this->render('paragraph/index.html.twig', ['paragraphs' => $paragraphRepository->findAll()]);
    }

    /**
     * @Route("/new", name="paragraph_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $paragraph = new Paragraph();
        $form = $this->createForm(ParagraphType::class, $paragraph);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($paragraph);
            $em->flush();

            return $this->redirectToRoute('paragraph_index');
        }

        return $this->render('paragraph/new.html.twig', [
            'paragraph' => $paragraph,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="paragraph_show", methods="GET")
     */
    public function show(Paragraph $paragraph): Response
    {
        return $this->render('paragraph/show.html.twig', ['paragraph' => $paragraph]);
    }

    /**
     * @Route("/{id}/edit", name="paragraph_edit", methods="GET|POST")
     */
    public function edit(Request $request, Paragraph $paragraph): Response
    {
        $form = $this->createForm(ParagraphType::class, $paragraph);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('paragraph_index', ['id' => $paragraph->getId()]);
        }

        return $this->render('paragraph/edit.html.twig', [
            'paragraph' => $paragraph,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="paragraph_delete", methods="DELETE")
     */
    public function delete(Request $request, Paragraph $paragraph): Response
    {
        if ($this->isCsrfTokenValid('delete'.$paragraph->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($paragraph);
            $em->flush();
        }

        return $this->redirectToRoute('paragraph_index');
    }
}
