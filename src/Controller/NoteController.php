<?php

namespace App\Controller;

use App\Entity\Link;
use App\Entity\Note;
use App\Entity\Paragraph;
use App\Entity\ToDoList;
use App\Form\NoteType;
use App\Repository\NoteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/note")
 */
class NoteController extends AbstractController
{
    /**
     * @Route("/", name="note_index", methods="GET")
     */
    public function index(NoteRepository $noteRepository): Response
    {
        return $this->render('note/index.html.twig', ['notes' => $noteRepository->findAll()]);
    }

    /**
     * @Route("/new", name="note_new", methods="GET|POST")
     */
    public function new(Request $request, NoteRepository $noteRepository): Response
    {
        $note = new Note();
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $paragraphs = $note->getParagraphs();
            foreach ($paragraphs as $paragraph) {
                $paragraph->setNote($note);
                $em->persist($paragraph);
            }

            $toDoLists = $note->getToDoLists();
            foreach ($toDoLists as $toDoList) {
                $toDoList->setNote($note);
                $em->persist($toDoList);
            }

            $links = $note->getLinks();
            foreach ($links as $link) {
                $link->setNote($note);
                $em->persist($link);
            }


            $em->persist($note);
            $em->flush();
            return $this->redirectToRoute('note_edit', ['id' => $note->getId()]);
        }

        return $this->render('note/new.html.twig', [
            'note' => $note,
            'notes' => $noteRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="note_show", methods="GET")
     */
    public function show(Note $note): Response
    {
        return $this->render('note/show.html.twig', ['note' => $note]);
    }

    /**
     * @Route("/{id}/edit", name="note_edit", methods="GET|POST")
     */
    public function edit(Request $request, Note $note, NoteRepository $noteRepository): Response
    {
        $originalParagraphs = new ArrayCollection();

        foreach ($note->getParagraphs() as $paragraph) {
            $originalParagraphs->add($paragraph);
        }

        $originalToDoLists = new ArrayCollection();

        foreach ($note->getToDoLists() as $toDoList) {
            $originalToDoLists->add($toDoList);
        }

        $originalLinks = new ArrayCollection();

        foreach ($note->getLinks() as $link) {
            $originalLinks->add($link);
        }



        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($originalParagraphs as $originalParagraph) {
                if ($note->getParagraphs()->contains($originalParagraph) === false) {
                    $note->getParagraphs()->removeElement($originalParagraph);
                    $em->remove($originalParagraph);
                }
            }

            $paragraphs = $note->getParagraphs();
            foreach ($paragraphs as $paragraph) {
                $paragraph->setNote($note);
                $em->persist($paragraph);
            }

            foreach ($originalToDoLists as $originalToDoList) {
                if ($note->getToDoLists()->contains($originalToDoList) === false) {
                    $note->getToDoLists()->removeElement($originalToDoList);
                    $em->remove($originalToDoList);
                }
            }

            $toDoLists = $note->getToDoLists();
            foreach ($toDoLists as $toDoList) {
                $toDoList->setNote($note);
                $em->persist($toDoList);
            }

            foreach ($originalLinks as $originalLink) {
                if ($note->getLinks()->contains($originalLink) === false) {
                    $note->getLinks()->removeElement($originalLink);
                    $em->remove($originalLink);
                }
            }

            $links = $note->getLinks();
            foreach ($links as $link) {
                $link->setNote($note);
                $em->persist($link);
            }

            $em->persist($note);
            $em->flush();

            return $this->redirectToRoute('note_edit', ['id' => $note->getId()]);
        }

        return $this->render('note/edit.html.twig', [
            'note' => $note,
            'notes' => $noteRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="note_delete", methods="DELETE")
     */
    public function delete(Request $request, Note $note): Response
    {
        if ($this->isCsrfTokenValid('delete'.$note->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();

            $paragraphs = $note->getParagraphs();
            foreach ($paragraphs as $paragraph) {
                $em->remove($paragraph);
            }

            $toDoLists = $note->getToDoLists();
            foreach ($toDoLists as $toDoList) {
                $em->remove($toDoList);
            }

            $links = $note->getLinks();
            foreach ($links as $link) {
                $em->remove($link);
            }

            $em->remove($note);
            $em->flush();
        }

        return $this->redirectToRoute('note_new');
    }

    /**
     * @Route("/{id}", name="note_duplicate", methods="GET|POST")
     */
    public function duplicate(Request $request, NoteRepository $noteRepository, Note $note): Response
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        $duplicatedNote = new Note();
        $duplicatedNote->setTitle($note->getTitle());

        $paragraphs = $note->getParagraphs();
        $toDoLists = $note->getToDoLists();
        $links = $note->getLinks();

        foreach ($paragraphs as $paragraph) {
            $p = new Paragraph();
            $p->setTitle($paragraph->getTitle());
            $p->setContent($paragraph->getContent());
            $p->setNote($duplicatedNote);
            $duplicatedNote->addParagraph($p);
            $em->persist($p);
        }

        foreach ($toDoLists as $toDoList) {
            $tdl = new ToDoList();
            $tdl->setTitle($toDoList->getTitle());
            $tdl->setDone($toDoList->getDone());
            $tdl->setNote($duplicatedNote);
            $duplicatedNote->addToDoList($tdl);
            $em->persist($tdl);
        }

        foreach ($links as $link) {
            $l = new Link();
            $l->setTitle($link->getTitle());
            $l->setUrl($link->getUrl());
            $l->setNote($duplicatedNote);
            $duplicatedNote->addLink($l);
            $em->persist($l);
        }

        $em->persist($duplicatedNote);
        $em->flush();

        return $this->render('note/edit.html.twig', [
            'note' => $duplicatedNote,
            'notes' => $noteRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }
}
