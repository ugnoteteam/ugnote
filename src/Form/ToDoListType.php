<?php

namespace App\Form;

use App\Entity\ToDoList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ToDoListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('done')
            ->add('title', TextType::class, array('attr' => array('placeholder' => 'Task', 'autocomplete' => 'off')))
            ->add('tasks', CollectionType::class, array('entry_type' => TaskType::class, 'entry_options' => array('label' => false), 'allow_add' => true, 'allow_delete' => true))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ToDoList::class,
        ]);
    }
}
