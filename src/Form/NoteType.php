<?php

namespace App\Form;

use App\Entity\Note;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('attr' => array('placeholder' => 'Title', 'autocomplete' => 'off')))
            ->add('paragraphs', CollectionType::class, array('entry_type' => ParagraphType::class, 'entry_options' => array('label' => false), 'allow_add' => true, 'allow_delete' => true))
            ->add('toDoLists', CollectionType::class, array('entry_type' => ToDoListType::class, 'entry_options' => array('label' => false), 'allow_add' => true, 'allow_delete' => true))
            ->add('links', CollectionType::class, array('entry_type' => LinkType::class, 'entry_options' => array('label' => false), 'allow_add' => true, 'allow_delete' => true))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Note::class,
        ]);
    }
}
